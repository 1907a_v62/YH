module.exports = {
    // es5 对象或者数组末尾加逗号
    "trailingComma" : 'es5',
    // 缩进
    "tabWidth": 4,
    // 句末不加分号
    "semi": false,
    // 不启用双引号
    "singleQuote": true ,
    // 对象，数组与文字之间加空格
    "bracketSpacing" : true,
    // 箭头函数单一参数省略括号
    "arrowParens" : "avoid" ,
    // 换行长度 100
    "printWidth": 100,
    // 括号线 > 放在末尾
    "bracketSpacing" : true 
}
