import { Suspense } from 'react'

export const Load = children => {
    const Com = children.el || children

    return <Suspense fallback={''}>{<Com to={children.url || void 0} />}</Suspense>
}
