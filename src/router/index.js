import { useRoutes } from 'react-router-dom'

import publicRoutes from './router.config'

const Route = () => useRoutes(publicRoutes)

export default Route
