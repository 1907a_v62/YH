import { Navigate } from 'react-router-dom'

import { lazy } from 'react'

import { Load } from './router.utils'

const [Home, Article, Pigeonhole, Knowledge, Message, About, NotFound] = [
    lazy(() => import('@/view/Home')),

    lazy(() => import('@/view/Home/Article')),

    lazy(() => import('@/view/Home/Pigeonhole')),

    lazy(() => import('@/view/Home/Knowledge')),

    lazy(() => import('@/view/Home/Message')),

    lazy(() => import('@/view/Home/About')),

    lazy(() => import('@/view/404')),
]

const publicRoutes = [
    {
        path: '/',
        element: Load({
            el: Navigate,
            url: '/home',
        }),
    },
    {
        path: '/home',
        element: Load(Home),
        children: [
            {
                path: '/home',
                element: Load({
                    el: Navigate,
                    url: '/home/article',
                }),
            },
            {
                path: '/home/article',
                element: Load(Article),
                meta: {
                    title: '文章',
                },
            },
            {
                path: '/home/pigeonhole',
                element: Load(Pigeonhole),
                meta: {
                    title: '归档',
                },
            },
            {
                path: '/home/knowledge',
                element: Load(Knowledge),
                meta: {
                    title: '知识小册',
                },
            },
            {
                path: '/home/message',
                element: Load(Message),
                meta: {
                    title: '留言板',
                },
            },
            {
                path: '/home/about',
                element: Load(About),
                meta: {
                    title: '关于',
                },
            },
            {
                path: '/home/*',
                element: Load(NotFound),
            },
        ],
    },
    {
        path: '*',
        element: Load({
            el: Navigate,
            url: '/home/*',
        }),
    },
]

export default publicRoutes
