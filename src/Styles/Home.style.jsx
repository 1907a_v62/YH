import styled from 'styled-components'

export const Container = styled.div`
    width: 100%;
    height: 100%;
    display: grid;
    grid-template-rows: 100px 1fr;
    background: #e7eaee;
`

export const Main = styled.main`
    width: 100%;
    height: 100%;
    overflow: auto;
`
export const Header = styled.header`
    width: 100%;
    height: 100%;
    position: relative;
    transition: all 0.3s ease-in-out;
    background: #fff;
    z-index: 10000;
    .container {
        width: 1800px;
        height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
        position: absolute;
        left: 300px;
        right: 300px;
        margin: auto;
    }
`

export const HeaderLogo = styled.div`
    display: inline-flex;
    align-items: center;
    height: 100%;
    margin-right: 4rem;
    color: rgba(0, 0, 0, 0.85);
    line-height: 64px;
    text-align: left;
    justify-content: center;
    align-items: center;
    a {
        display: flex;
        align-items: center;
        display: block;
        width: 100%;
        height: 60px;
        img {
            max-width: 100%;
            height: 100%;
            vertical-align: middle;
            border-style: none;
        }
    }
`

export const HeaderRight = styled.div`
    display: flex;
    flex: 1;
    justify-content: space-between;
    align-items: center;
    color: rgba(0, 0, 0, 0.85);
    font-size: 16px;
    height: 100%;
`
