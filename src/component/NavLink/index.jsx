import { NavLink } from 'react-router-dom'

import { Link } from './index.style'

const NavList = ({ routes }) => (
    <Link>
        {routes && routes.length > 0 ? (
            routes.map(item => (
                <NavLink
                    key={item.path}
                    to={item.path}
                    className={({ isActive }) => (isActive ? 'selected' : '')}
                >
                    {item?.meta?.title}
                </NavLink>
            ))
        ) : (
            <div>暂无数据</div>
        )}
    </Link>
)

export default NavList
