import { Container, Header, Main, HeaderLogo, HeaderRight } from '@/Styles/Home.style'

import publicRoutes from '@/router/router.config'

import { matchRoutes, useLocation, Outlet } from 'react-router-dom'

import NavList from '@/component/NavLink'

const Home = () => {
    const location = useLocation()

    const Routes = matchRoutes(publicRoutes, location)
        .filter(item => item.pathname === '/home')[0]
        .route.children.filter(item => item.path !== '/home' && item.path !== '/home/*')

    return (
        <Container>
            <Header>
                <div className="container">
                    <HeaderLogo>
                        <a aria-label="home" href="/">
                            <img
                                height="36"
                                src="https://wipi.oss-cn-shanghai.aliyuncs.com/2021-02-20/wipi-logo.png"
                                alt="logo"
                            />
                        </a>
                    </HeaderLogo>
                    <HeaderRight>
                        <NavList routes={Routes} />
                    </HeaderRight>
                </div>
            </Header>
            <Main>
                <Outlet />
            </Main>
        </Container>
    )
}

export default Home
