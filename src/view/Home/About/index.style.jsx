import styled from 'styled-components'
export const Container = styled.div`
    width: 100%;
    height: 1000px;
    /* background: #ccc; */

    .img img {
        width: 768px;
        height: 521px;
        margin-left: 20%;
    }
    h1 {
        margin-top: 20px;
        width: 71%;
        height: 50px;
        margin-left: 20%;
        text-align: center;
    }
    div h2 {
        width: 71%;
        height: 50px;
        margin-left: 20%;
    }
    hr {
        width: 70%;
        margin-left: 20%;
    }
    li {
        width: 70%;
        margin-left: 20%;
    }
    .pinglun {
        margin-top: 20px;
        width: 100%;
        height: 641px;
        /* background: greenyellow; */
        h5 {
            text-align: center;
            font-size: 24px;
        }
    }
    .ant-input {
        height: 142px;
        min-height: 142px;
        max-height: 274px;
        overflow-y: hidden;
        resize: none;
    }
    footer {
        width: 100%;
        height: 50px;
        line-height: 50px;
        /* background-color: orange; */
    }
    footer path {
        float: left;
    }
    footer span {
        float: left;
    }
    footer ol {
        float: right;
        width: 50px;
        height: 30px;
        margin-right: 20px;
        background-color: #ccc;
        text-align: center;
        line-height: 30px;
        margin-top: 10px;
    }
    .ant-avatar ant-avatar-circle {
        width: 32px;
        height: 32px;
        line-height: 32px;
        font-size: 18px;
        background-color: rgb(255, 0, 100);
    }
    .ant-avatar-string {
        line-height: 32px;
        transform: scale(1) translateX(-50%);
    }
    .qq {
        padding: 12px 0px 12px 40px;
    }
    .ww {
        padding-left: 40px;
    }
    .anticon anticon-message {
        margin-right: 4px;
    }
    .A5 {
        background: #fff;
    }
    .ee {
        width: 100%;
        height: 50px;
        background: #fff;
        margin-top: 20px;
    }
    .ee span {
        width: 50px;
        height: 50px;
        text-align: center;
        line-height: 50px;
        background: #ccc;
        float: left;
        margin: 0 10px;
    }
    .rr {
        margin-left: 45%;
    }
    .tuijian {
        width: 100%;
        height: 400px;
        /* background: greenyellow; */
    }
    .tuijian h4 {
        width: 100%;
        height: 50px;
        text-align: center;
        font-size: 24px;
        background: #fff;
    }
    .div {
        opacity: 1;
    }
    .qv {
        border-bottom: 1px solid #ccc;
        background: #fff;
    }
    .span {
        color: red;
    }
`
