module.exports = {
    // 不往父级查找
    root: true,
    // 环境配置
    env: {
        node: true,
        browser: true,
        es6: true,
    },
    // 拓展规则
    extends: ['react-app', 'react-app/jest', 'prettier'],
    // 自定义规则，会覆盖一部分拓展规则
    // 具体这些参数代表什么规则，可以去eslint官网看
    rules: {
        // 不能存留console.log()
        'no-console': 'warn',
        // 尽可能的使用单引号
        quotes: ['error', 'single'],
        // 禁止出现空语句块
        'no-empty': ['error', { allowEmptyCatch: true }],
        // 禁止对function进行重复赋值
        'no-func-assign': 'error',
        // 禁止出现令人困惑的多行表达式
        'no-unexpected-multiline': 'error',
        // 禁止在 return、throw、continue 和 break 语句之后出现不可达代码
        'no-unreachable': 'error',
        // switch 必须要有default
        'default-case': 'error',
        // 禁止使用原生alert，confirm,prompt
        'no-alert': 'error',
        // 禁用arguments.caller callee
        'no-caller': 'error',
        // 不能使用空解构
        'no-empty-pattern': 'error',
        // 避免使用eval
        'no-implied-eval': 'error',
        // 禁止自己对自己赋值
        'no-self-assign': 'error',
        // 关闭变量未使用的情况
        'no-unused-vars': 'off',
        // 关闭一些超链接无效
        'jsx-a11y/anchor-is-valid': 'off',
    },
    // 语言风格
    parserOptions: {
        // 支持import
        sourceType: 'module',
    },
}
