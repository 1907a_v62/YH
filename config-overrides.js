const path = require('path')

const { fixBabelImports, override, addLessLoader, adjustStyleLoaders } = require('customize-cra')

const webpack = (config, env) => {
    config.resolve.alias = {
        '@': path.join(__dirname, 'src'),
    }

    config.module.rules = [
        ...config.module.rules,
        ...[
            {
                test: /\.(json)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                    },
                },
            },
        ],
    ]

    return config
}

module.exports = override(
    webpack,
    fixBabelImports('import', {
        libraryName: 'antd',
        libraryDirectory: 'es',
        style: true, //自动打包相关的样式 默认为 style:'css'
    }),
    addLessLoader({
        lessOptions: {
            localIdentName: '[local]--[hash:base64:5]',
            javascriptEnabled: true,
            modifyVars: { '@primary-color': '#1DA57A' },
        },
    }),
    adjustStyleLoaders(({ use: [, , postcss] }) => {
        const postcssOptions = postcss.options
        postcss.options = { postcssOptions }
    })
)
